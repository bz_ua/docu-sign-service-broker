﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using SourceCode.Hosting.Client.BaseAPI;
using SSC = SourceCode.SmartObjects.Client;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.IO;

namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("Document", "Document", "Document")]
    class Document
    {
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }

        private string envelopeID = "";
        private string _document="";
        private string documentID = "";
        private string smart_object_name = "";
        private string group_key_name = "";
        private string group_key_value = "";
        private string return_column = "";
        private string return_document_name = "";
        private string json_filters = "";

        #region Service Instance Fields
        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName; }
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }

        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }
        #endregion

        #region SMO Properties

        [Attributes.Property("ReturnDocumentName", SoType.Text, "ReturnDocumentName", "ReturnDocumentName")]
        public string ReturnDocumentName
        {
            get { return return_document_name; }
            set { return_document_name = value; }
        }
        [Attributes.Property("ReturnColumn", SoType.Text, "ReturnColumn", "ReturnColumn")]
        public string ReturnColumn
        {
            get { return return_column; }
            set { return_column = value; }
        }

        [Attributes.Property("JSONFilters", SoType.Text, "JSONFilters", "JSONFilters")]
        public string JSONFilters
        {
            get { return json_filters; }
            set { json_filters = value; }
        }

        [Attributes.Property("GroupKeyName", SoType.Text, "GroupKeyName", "GroupKeyName")]
        public string GroupKeyName
        {
            get { return group_key_name; }
            set { group_key_name = value; }
        }

        [Attributes.Property("GroupKeyValue", SoType.Text, "GroupKeyValue", "GroupKeyValue")]
        public string GroupKeyValue
        {
            get { return group_key_value; }
            set { group_key_value = value; }
        }

        [Attributes.Property("SmartObjectName", SoType.Text, "SmartObjectName", "SmartObjectName")]
        public string SmartObjectName
        {
            get { return smart_object_name; }
            set { smart_object_name = value; }
        }

        [Attributes.Property("DocumentID", SoType.Text, "DocumentID", "DocumentID")]
        public string DocumentID
        {
            get { return documentID; }
            set { documentID = value; }
        }

        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }

        [Attributes.Property("Document", SoType.File, "Document", "Document")]
        public string ReturnedDocument
        {
            get { return _document; }
            set { _document = value; }
        }


        #endregion
       
        #region Load Method
        [Attributes.Method("Load", MethodType.Read, "Load", "Load Document",
           new string[] { "EnvelopeID", "DocumentID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
           new string[] { "EnvelopeID", "DocumentID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
           new string[] { "Document" })]

        public Document Load()
        {
            string file_name = "";

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader,BaseUrl + "/envelopes/" + EnvelopeID + "/documents").Result;

            if(result.StatusCode == HttpStatusCode.OK)
            {
                JObject envelope_document = JObject.Parse(Help.ReadContentAsString(result).Result);

                foreach (var subobj in envelope_document["envelopeDocuments"])
                {
                    if (subobj["documentId"].ToString() == DocumentID)
                    {
                        file_name = subobj["name"].ToString();
                        break;
                    }
                }
            }
            else
            {
                Help.RaiseException(result);
            }

            
           HttpResponseMessage attached_file_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/documents/" + DocumentID).Result;

            if(attached_file_response.StatusCode==HttpStatusCode.OK)
            {
                byte[] attached_file = Help.ReadContentAsByteArray(attached_file_response).Result;
                string base64Code = Convert.ToBase64String(attached_file);
                XElement exFile = new XElement("file",
                        new XElement("name", file_name),
                        new XElement("content", base64Code)
                    );
                this.ReturnedDocument = exFile.ToString();
            }
            else
            {
                Help.RaiseException(attached_file_response);
            }
            
            return this;
        }

        #endregion

        [Attributes.Method("MergeDocuments", MethodType.Read, "Merge Documents", "Merge Documents",
          new string[] { "SmartObjectName", "ReturnColumn", "ReturnDocumentName" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
          new string[] { "SmartObjectName", "JSONFilters", "ReturnColumn", "GroupKeyName", "GroupKeyValue", "ReturnDocumentName" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
          new string[] { "Document" })]

        public Document MergeDocuments()
        {

            XElement attached_file_xml;
            List<byte[]> files = new List<byte[]>();
            byte[] fileContents = null;
            Stream s;
            Dictionary<string, List<string>> filters = new Dictionary<string, List<string>>();
            JObject json_filters;

            if (!String.IsNullOrEmpty(JSONFilters))
            {
                json_filters = JObject.Parse(JSONFilters);
              
                foreach (var filter in json_filters["Filters"])
                {
                    filters.Add(filter["Name"].ToString(), filter["Value"].Select(i => i.ToString()).ToList());
                }
            }
           




            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder hostServerConnectionString = new SCConnectionStringBuilder();
            hostServerConnectionString.Host = "localhost"; // name of the K2 host server, or the name of the DNS entry pointing to the K2 Farm
            hostServerConnectionString.Port = 5555; // use port 5555 for all non-workflow client connections
            hostServerConnectionString.IsPrimaryLogin = true; // true = re-authenticate user, false = use cached security credentials
            hostServerConnectionString.Integrated = true; // tr


            // open a K2 Server connection
            SourceCode.SmartObjects.Client.SmartObjectClientServer serverName = new SSC.SmartObjectClientServer();
            serverName.CreateConnection();
            serverName.Connection.Open(hostServerConnectionString.ToString());
        
            try {
                SSC.SmartObject smo = serverName.GetSmartObject(SmartObjectName);
                smo.MethodToExecute = "List";

                if(!String.IsNullOrEmpty(GroupKeyValue))
                     smo.Properties[GroupKeyName].Value = GroupKeyValue;


                SSC.SmartObjectList smoList = serverName.ExecuteList(smo);

                if(filters.Keys.Count>0)
                {
                    foreach (KeyValuePair<string, List<string>> filter in filters)
                    {
                        foreach (var filter_value in filter.Value)
                        {
                            foreach (SSC.SmartObject resultingSmO in smoList.SmartObjectsList)
                            {
                                try
                                {
                                    if (resultingSmO.Properties[filter.Key].Value == filter_value)
                                    {
                                        attached_file_xml = XElement.Parse(resultingSmO.Properties[ReturnColumn].Value);
                                        files.Add(Convert.FromBase64String(attached_file_xml.Element("content").Value));
                                    }

                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error parsing XML. " + ex.Message);
                                }

                            }
                        }

                    }
                }
                else
                {
                    foreach (SSC.SmartObject resultingSmO in smoList.SmartObjectsList)
                    {
                        try
                        {
                                attached_file_xml = XElement.Parse(resultingSmO.Properties[ReturnColumn].Value);
                                files.Add(Convert.FromBase64String(attached_file_xml.Element("content").Value));

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error parsing XML. " + ex.Message);
                        }

                    }
                }
              
               

            } catch (Exception ex)
            {
                throw new Exception("ERROR: "+ex.Message);
            }
            finally
            {
                // close the connection
                serverName.Connection.Close();
            }

            if(files.Count>0)
            {
             
                using (PdfDocument output_pdf = new PdfDocument())
                {
                    foreach (var file in files)
                    {
                        s = new MemoryStream(file);
                        using (PdfDocument pdf = PdfReader.Open(s, PdfDocumentOpenMode.Import))
                        {
                            for (int i = 0; i < pdf.PageCount; i++)
                            {
                                output_pdf.AddPage(pdf.Pages[i]);
                            }
                        }
                    }

                   
                    using (MemoryStream stream = new MemoryStream())
                    {
                        output_pdf.Save(stream, true);
                        fileContents = stream.ToArray();
                    }

                }
                   
            }
            string file_name = ReturnDocumentName;

            
                string base64Code = Convert.ToBase64String(fileContents);
                XElement exFile = new XElement("file",
                        new XElement("name", file_name),
                        new XElement("content", base64Code)
                    );
                this.ReturnedDocument = exFile.ToString();
       

            return this;
        }
    }
}
