﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json;


using SourceCode.SmartObjects.Services.ServiceSDK;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

using Bytezoom.Services.DocuSign.Interfaces;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Bytezoom.Services.DocuSign.Data
{
    /// <summary>
    /// An implementation of static IDataConnector responsible for interacting with an underlying system or technology. 
    /// The purpose of this class is to expose and represent the underlying data and services as Service Objects 
    /// which are, in turn, consumed by K2 SmartObjects
    /// TODO: implement the Interface Methods with your own code
    /// </summary>
    class DataConnector : IDataConnector
    {
        #region Class Level Fields

        #region Constants
        /// <summary>
        /// Constant for the Type Mappings configuration lookup in the service instance.
        /// </summary>
       // private const string TYPEMAPPINGS = "Type Mappings";
        #endregion
        
        #region Private Fields
        /// <summary>
        /// Local serviceBroker variable.
        /// </summary>
        private ServiceAssemblyBase serviceBroker = null;

        /// <summary>
        /// Sample configuration values for the service instance
        /// defined by the SetupConfiguration() method and set by the GetConfiguration() method
        /// </summary>
       // private string requiredConfigurationValue = string.Empty;
       // private string optionalConfigurationValue = string.Empty;
        private static string docu_sign_url = string.Empty;
        private static string account_id = string.Empty;
        private static string integrator_key = string.Empty;
        private static bool flag = false;
        private static string responseBody = "";
        private static string base_url = "";

        public static string AccountID
        {
            get {return account_id;}
            set { account_id = value; }
        }
        public static string DocuSignUrl
        {
            get { return docu_sign_url; }
        }

        public static string IntegratorKey
        {
            get { return integrator_key; }
        }
        public static string BaseUrl
        {
            get { return base_url; }
        }
      

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new DataConnector.
        /// </summary>
        /// <param name="serviceBroker">The ServiceBroker.</param>
        public DataConnector(ServiceAssemblyBase serviceBroker)
        {
            this.serviceBroker = serviceBroker;
           
        }
        #endregion

        #region Methods

        #region void SetupConfiguration()
     
        public void SetupConfiguration()
        {
          
            serviceBroker.Service.ServiceConfiguration.Add("DocuSign URL", true, "https://demo.docusign.net");
            serviceBroker.Service.ServiceConfiguration.Add("Integrator Key", true, string.Empty);
            serviceBroker.Service.ServiceConfiguration.Add("Base Url", false, string.Empty);
            serviceBroker.Service.ServiceConfiguration.Add("Base Url", false, string.Empty);
            serviceBroker.Service.ServiceConfiguration.Add("ReycleBin Folder ID", false, string.Empty);
        }
        #endregion

        #region void GetConfiguration()
        /// <summary>
        /// Retrieves the configuration from the service instance and stores the retrieved configuration in local variables for later use.
        /// </summary>
        public void GetConfiguration()
        {
            if (serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.AuthenticationMode != AuthenticationMode.Static)
                throw new Exception("\nAuthentication Mode must be Static.");

            docu_sign_url = serviceBroker.Service.ServiceConfiguration["DocuSign URL"].ToString();

            integrator_key = serviceBroker.Service.ServiceConfiguration["Integrator Key"].ToString();

            HttpResponseMessage login_information_response = Help.GetBaseUrlResponse(Help.GetAuthHeader(serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.UserName, serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.Password,integrator_key),docu_sign_url).Result;
            if(login_information_response.IsSuccessStatusCode)
            {
                JObject login_information = JObject.Parse(Help.ReadContentAsString(login_information_response).Result);
                AccountID = login_information["loginAccounts"][0]["accountId"].ToString();
                base_url = login_information["loginAccounts"][0]["baseUrl"].ToString();
                serviceBroker.Service.ServiceConfiguration["Base Url"] = base_url;
            }
          else
            {
                Help.RaiseException(login_information_response);
            }

            string recylebindID = "";

            HttpResponseMessage folders_response = Help.RunAsyncGetResponse(Help.GetAuthHeader(serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.UserName, serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.Password, integrator_key), BaseUrl + "/folders/").Result;
            if (folders_response.IsSuccessStatusCode)
            {
                JObject Folders = JObject.Parse(Help.ReadContentAsString(folders_response).Result);
                if (Folders["folders"] != null)
                    foreach (var item in Folders["folders"])
                    {
                        if (item["type"].ToString() == "recyclebin")
                        {
                            serviceBroker.Service.ServiceConfiguration["ReycleBin Folder ID"] = item["folderId"].ToString();
                            break;
                        }
                    }
            }
            else
            {
                Help.RaiseException(folders_response);
            }


        }
        #endregion
        

        #region void SetupService()
        /// <summary>
        /// Sets up the service instance's default name, display name, and description.
        /// </summary>
        public void SetupService()
        {
            //TODO: Add code to set up the service instance name, display name and description

            //In this example, we are setting the Name, DisplayName and Description to the Project namespace
            //NOTE: "Name" cannot contain spaces
            serviceBroker.Service.Name = this.GetType().Namespace;
           
            string friendlyName = this.GetType().Assembly.GetName().Name + "." + this.GetType().Name;
            //display name is shown to the user, so let's use a unique but friendly name
            serviceBroker.Service.MetaData.DisplayName = friendlyName + " (Display Name)";
            serviceBroker.Service.MetaData.Description = friendlyName + " (Description)";
        }
        #endregion

        #region void DescribeSchema()
        /// <summary>
        /// Describes the schema of the underlying data and services to the K2 platform.
        /// </summary>
        public void DescribeSchema()
        {
            //TODO: Since this is a static broker, you would add static service objects using attribute decoration.
            //The recommended approach is to create separate classes for each of your static service objects

            //in the sample implementation, we iterate over each of the classes in the assembly and if they are decorated with the 
            //ServiceObjectAttribute, we add them as service objects. 
            //if you prefer, you can manually add Service Objects like this instead: 
            //serviceBroker.Service.ServiceObjects.Add(new ServiceObject(typeof(StaticServiceObject1)));

            Type[] types = this.GetType().Assembly.GetTypes();

            foreach (Type t in types)
            {
                if (t.IsClass)
                {
                    if (t.GetCustomAttributes(typeof(SourceCode.SmartObjects.Services.ServiceSDK.Attributes.ServiceObjectAttribute), false).Length > 0)
                    {
                        this.serviceBroker.Service.ServiceObjects.Add(new SourceCode.SmartObjects.Services.ServiceSDK.Objects.ServiceObject(t));
                    }
                }
            }
        }
        #endregion

        #region void Dispose()
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //TODO: Add any additional IDisposable implementation code here. Make sure to dispose of any data connections.

            // Clear reference to serviceBroker.
            serviceBroker = null;
        }
        #endregion
        #endregion
    }
   
}