﻿using System;
using System.Collections.Generic;
using System.Text;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;

namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("PowerForm", "PowerForm", "PowerForm Service Object provide methods to work with DocuSign PowerForms")]
    class PowerForm
    {

        public class PowerFormJ
        {
            [JsonProperty("powerFormId")]
            public string powerFormId { get; set; }

            [JsonProperty("envelopes")]
            public string[] envelopes { get; set; }

        }
        public class PowerFormJson
        {
            [JsonProperty("powerForm")]
            public PowerFormJ PowerForm { get; set; }
        }


        #region Service Instance Fields
        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName; }
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }

        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }
        #endregion


        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
           set { _serviceConfig = value; }
        }

 
        #region Class Level Fields

      
        private string status = "";

        private string powerFormURL = "";
        private string jsonResult = "";
        private string powerFormID = "";
        private string templateID = "";
        private string envelopeID = "";
        private string powerFormName = "";
        private string powerFormEmailBody = "";
        private string powerFormEmailSubject = "";

        #endregion

        #region SMO Properties
        [Attributes.Property("JSONResult", SoType.Text, "JSONResult", "JSONResult")]
        public string JSONResult
        {
            get { return jsonResult; }
            set { jsonResult = value; }
        }


        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }

        [Attributes.Property("TemplateID", SoType.Text, "TemplateID", "TemplateID")]
        public string TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }


        [Attributes.Property("PowerFormEmailSubject", SoType.Text, "PowerFormEmailSubject", "PowerFormEmailSubject")]
        public string PowerFormEmailSubject
        {
            get { return powerFormEmailSubject; }
            set { powerFormEmailSubject = value; }
        }

        [Attributes.Property("PowerFormEmailBody", SoType.Text, "PowerFormEmailBody", "PowerFormEmailBody")]
        public string PowerFormEmailBody
        {
            get { return powerFormEmailBody; }
            set { powerFormEmailBody = value; }
        }

        [Attributes.Property("PowerFormName", SoType.Text, "PowerFormName", "PowerFormName")]
        public string PowerFormName
        {
            get { return powerFormName; }
            set { powerFormName = value; }
        }


        [Attributes.Property("PowerFormURL", SoType.Text, "PowerFormURL", "PowerFormURL")]
        public string PowerFormURL
        {
            get { return powerFormURL; }
            set { powerFormURL = value; }
        }

        [Attributes.Property("PowerFormID", SoType.Text, "PowerFormID", "PowerFormID")]
        public string PowerFormID
        {
            get { return powerFormID; }
            set { powerFormID = value; }
        }

        [Attributes.Property("Status", SoType.Text, "Status", "Status")]
        public  string Status
        {
            get { return status; }
            set { status = value; }
        }

        #endregion
        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public PowerForm()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region Create Power Form
        [Attributes.Method("Create", MethodType.Read, "Create", "Create",
            new string[] { "PowerFormName",  "TemplateID"}, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "PowerFormName", "TemplateID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "Status", "PowerFormURL", "PowerFormID" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm Create()
        {

            HttpResponseMessage result = Help.RunAsyncPostResponse(AuthHeader,BaseUrl + "/powerforms",
                new StringContent(
                 "{ \"name\": \""+PowerFormName+"\",\"isActive\": \"true\",\"maxUseEnabled\": \"false\",\"signingMode\": \"direct\",\"templateId\": \""+TemplateID+"\"}", Encoding.UTF8, "application/json")
                ).Result;

            if(result.IsSuccessStatusCode)
            {
                JObject power_form = JObject.Parse(Help.ReadContentAsString(result).Result);

                this.PowerFormID = power_form["powerFormId"].ToString();
                this.PowerFormURL = power_form["powerFormUrl"].ToString();
                this.Status = "OK";

            }
            else
            {
                Help.RaiseException(result);
            }

            return this;
        }
        #endregion

        #region Get Envelope ID
        [Attributes.Method("GetEnvelopeID", MethodType.Read, "GetEnvelopeID", "GetEnvelopeID",
            new string[] { "PowerFormID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "PowerFormID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "EnvelopeID"})] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm GetEnvelopeID()
        {

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/powerforms/"+PowerFormID+"/form_data").Result;

            if(result.IsSuccessStatusCode)
            {
                JObject envelope_list = JObject.Parse(Help.ReadContentAsString(result).Result);
                foreach (var envelope in envelope_list["envelopes"])
                {
                    HttpResponseMessage envelope_info = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + envelope["envelopeId"].ToString()).Result;
                    if (envelope_info.IsSuccessStatusCode)
                    {
                        JObject envelope_info_json = JObject.Parse(Help.ReadContentAsString(envelope_info).Result);
                        if (envelope_info_json["status"].ToString() == "completed")
                        {
                            this.EnvelopeID = envelope["envelopeId"].ToString();
                            break;
                        }
                    }
                    else
                    {
                        Help.RaiseException(envelope_info);
                    }
                    
                }
            }
            else
            {
                Help.RaiseException(result);
            }

            return this;
        }
        #endregion

        #region Delete Envelope
        [Attributes.Method("Delete", MethodType.Read, "Delete", "Delete",
            new string[] { "PowerFormID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "PowerFormID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "Status" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm Delete()
        {


            HttpResponseMessage result = Help.RunAsyncDELETEResponse(AuthHeader, BaseUrl + "/powerforms/" + PowerFormID, null).Result;

            if (result.IsSuccessStatusCode)
            {
                this.Status = result.ReasonPhrase;
            }
            else
            {
                Help.RaiseException(result);
            }



            return this;
        }
        #endregion

        #region Get List of Envelope Ids
        [Attributes.Method("GetJSONEnvelopes", MethodType.Read, "Get JSON Envelopes", "Get JSON Envelopes",
            new string[] {  }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] {  }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "JSONResult" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm GetJSONEnvelopes()
        {
            Dictionary<string, List<string>> PowerFormEnvelopes = new Dictionary<string, List<string>>();
            JObject EnvelopeIds;
            HttpResponseMessage form_data_reponse;
            List<PowerFormJson> power_forms = new List<PowerFormJson>();

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader,BaseUrl + "/powerforms").Result;

            if(result.IsSuccessStatusCode)
            {
                JObject powerforms = JObject.Parse(Help.ReadContentAsString(result).Result);

                List<string> PowerFormIds = powerforms["powerForms"].Where(t => (int)t["timesUsed"] > 0).Select(t => t["powerFormId"].ToString()).ToList();

                foreach (var item in PowerFormIds)
                {
                        form_data_reponse = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/powerforms/" + item + "/form_data").Result;
                        if (form_data_reponse.IsSuccessStatusCode)
                        {
                            EnvelopeIds = JObject.Parse(Help.ReadContentAsString(form_data_reponse).Result);
                            if (EnvelopeIds["envelopes"] != null)
                            {
                                PowerFormJson Pfj = new PowerFormJson();
                                PowerFormJ powerformj = new PowerFormJ
                                {
                                    powerFormId = item,
                                    envelopes = EnvelopeIds["envelopes"].Select(t => (string)t["envelopeId"]).ToArray()
                                };
                                Pfj.PowerForm = powerformj;
                                power_forms.Add(Pfj);
                            }
                        }
                        else
                        {
                            Help.RaiseException(form_data_reponse);
                        }
                   
                }

                if (power_forms.Count > 0)
                {
                    this.JSONResult = JsonConvert.SerializeObject(power_forms, Formatting.Indented);
                }
            }
            else
            {
                Help.RaiseException(result);
            }
            return this;
        }
        #endregion

        #region Get List of PowerForm Ids
        [Attributes.Method("GetJSONPowerForms", MethodType.Read, "Get JSON PowerForms", "Get JSON PowerForms",
            new string[] { }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "JSONResult" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm GetJSONPowerForms()
        {
          


            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/powerforms/").Result;

            if(result.IsSuccessStatusCode)
            {
                JObject power_forms = JObject.Parse(Help.ReadContentAsString(result).Result);
                List<string> PowerFormIds = power_forms["powerForms"].Where(t => (int)t["timesUsed"] >0).Select(t=> (string)t["powerFormId"]).ToList();

                if (PowerFormIds.Count > 0)
                {
                    this.JSONResult = JsonConvert.SerializeObject(PowerFormIds, Formatting.Indented);
                }
            }
            else
            {
                Help.RaiseException(result);
            }


            return this;
        }
        #endregion

        #region GetPowerFormIDByEnvelopeID
        [Attributes.Method("GetPowerFormIDByEnvelopeID", MethodType.Read, "Get PowerFormID By EnvelopeID", "Get PowerFormID By EnvelopeID",
            new string[] { "EnvelopeID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "EnvelopeID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "PowerFormID" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public PowerForm GetPowerFormIDByEnvelopeID()
        {
            string template_id = "";
            HttpResponseMessage envelope_templates = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/templates").Result;

            if (envelope_templates.IsSuccessStatusCode)
            {
                JObject templates = JObject.Parse(Help.ReadContentAsString(envelope_templates).Result);
                if (templates["templates"] != null)
                {
                    if (templates["templates"].Count() > 0)
                    {
                        if (templates["templates"][0]["templateId"] != null)
                            template_id = templates["templates"][0]["templateId"].ToString();
                    }

                }
            }
            else
            {
                Help.RaiseException(envelope_templates);
            }

            if(!string.IsNullOrEmpty(template_id))
            {
                HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/powerforms/").Result;

                if (result.IsSuccessStatusCode)
                {
                    JObject power_forms = JObject.Parse(Help.ReadContentAsString(result).Result);
                    if(power_forms["powerForms"].Where(t => t["templateId"].ToString() == template_id).Any())
                        this.PowerFormID = power_forms["powerForms"].Where(t => t["templateId"].ToString() == template_id).Select(t => (string)t["powerFormId"]).First();
                }
                else
                {
                    Help.RaiseException(result);
                }
            }
            return this;
        }
        #endregion


        #endregion
    }
}