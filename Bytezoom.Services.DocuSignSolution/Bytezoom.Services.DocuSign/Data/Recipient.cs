﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("Recipient", "Recipient", "Recipient Service Object provide methods to work with DocuSign Recipient")]
    class Recipient
    {
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }

        #region Service Instance Fields
        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName; }
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }

        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }

        #endregion
        #region Class Level Fields



        private string envelopeID = "";
        private string envelope_status = "";
        private string name = "";
        private string _value = "";
        private string attached_file;
        private string recipient_id = "";
        private string file_name = "";

        private string tabID = "";
        private string tabLabel = "";
        private string jSONTabs = "";
        private string status = "";




        #endregion

        #region SMO properties
        [Attributes.Property("Status", SoType.Text, "Status", "Status")]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        [Attributes.Property("JSONTabs", SoType.Text, "JSONTabs", "JSONTabs")]
        public string JSONTabs
        {
            get { return jSONTabs; }
            set { jSONTabs = value; }
        }
        [Attributes.Property("TabID", SoType.Text, "TabID", "TabID")]
        public string TabID
        {
            get { return tabID; }
            set { tabID = value; }
        }
        [Attributes.Property("TabLabel", SoType.Text, "TabLabel", "TabLabel")]
        public string TabLabel
        {
            get { return tabLabel; }
            set { tabLabel = value; }
        }
        [Attributes.Property("RecipientID", SoType.Text, "RecipientID", "RecipientID")]
        public string RecipientID
        {
            get { return recipient_id; }
            set { recipient_id = value; }

        }
        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }
        #endregion

        #region Get Recipient Text Tabs
          [Attributes.Method("GetRecipientTabs", MethodType.List, "GetRecipientTabs", "GetRecipientTabs",
            new string[] { "EnvelopeID", "RecipientID" },
            new string[] { "EnvelopeID", "RecipientID" },
            new string[] { "TabLabel", "TabID" })]
        public List<Recipient> GetRecipientTabs()
        {
            List<Recipient> TextTabs = new List<Recipient>();

            HttpResponseMessage result =  Help.RunAsyncGetResponse( AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs" ).Result;

            if(result.IsSuccessStatusCode)
            {
                dynamic recipient_tabs = JObject.Parse(Help.ReadContentAsString(result).Result);

                foreach (var tab_type in recipient_tabs)
                {
                    foreach (var tabs in tab_type)
                    {
                        foreach (var tab in tabs)
                        {

                            Recipient rec = new Recipient();
                            rec.TabLabel = tab["tabLabel"].ToString();
                            rec.TabID = tab["tabId"].ToString();
                            TextTabs.Add(rec);
                            rec = null;
                        }
                    }
                }
            }
            else
            {
                Help.RaiseException(result);
            }
            
            return TextTabs;
        }


        #endregion

          #region Update Recipient Tabs
          [Attributes.Method("UpdateRecipientTabs", MethodType.Execute, "UpdateRecipientTabs", "UpdateRecipientTabs",
           new string[] { "EnvelopeID", "RecipientID","JSONTabs" },
           new string[] { "EnvelopeID", "RecipientID", "JSONTabs" },
           new string[] {  })]

          public Recipient UpdateRecipientTabsJSON() 
          {

            HttpResponseMessage result = Help.RunAsyncPUTResponse(AuthHeader,BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs",new StringContent(JSONTabs,Encoding.UTF8)).Result;
            if (result.IsSuccessStatusCode)
            {
                this.Status = result.ReasonPhrase;
            }
            else
            {
                Help.RaiseException(result);
            }
              return this;
          }
          #endregion

          #region Delete Recipient Tabs
          [Attributes.Method("DeleteRecipientTabs", MethodType.Execute, "DeleteRecipientTabs", "DeleteRecipientTabs",
           new string[] { "EnvelopeID", "RecipientID", "JSONTabs" },
           new string[] { "EnvelopeID", "RecipientID", "JSONTabs" },
           new string[] {"Status" })]

          public Recipient DeleteRecipientTabsJSON()
          {

            HttpResponseMessage result = Help.RunAsyncDELETEResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs",new StringContent(JSONTabs, Encoding.UTF8, "application/json")).Result;
            if(result.IsSuccessStatusCode)
            {
                this.Status = result.ReasonPhrase;
            }
            else
            {
                Help.RaiseException(result);
            }
              return this;
          }
          #endregion


          #region Delete Signle Tab
          [Attributes.Method("DeleteSingleTab", MethodType.Execute, "DeleteSingleTab", "DeleteSingleTab",
            new string[] { "EnvelopeID", "RecipientID", "TabLabel" },
            new string[] { "EnvelopeID", "RecipientID", "TabLabel" },
            new string[] { "Status" })]
          public Recipient DeleteSingleTab()
          {

            string tab_type="";
            string tab_id="";
            bool flag_tab_types;
            bool flag_tabs;

            HttpResponseMessage recipient_tabs_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs").Result;
            
            if(recipient_tabs_response.IsSuccessStatusCode)
            {
                dynamic recipient_tabs = JObject.Parse(Help.ReadContentAsString(recipient_tabs_response).Result);

                foreach (var tab_types in recipient_tabs)
                {
                    flag_tab_types = false;
                    foreach (var tabs in tab_types)
                    {
                        flag_tabs = false;
                        foreach (var tab in tabs)
                        {
                            if (tab["tabLabel"].ToString() == TabLabel)
                            {
                                tab_type = tab_types.Name;
                                tab_id = tab["tabId"].ToString();
                                flag_tab_types = true;
                                flag_tabs = true;
                                break;
                            }
                        }
                        if (flag_tabs)
                            break;
                    }
                    if (flag_tab_types)
                        break;
                }

                this.Status = "Empty";

                if (!String.IsNullOrEmpty(tab_type))
                {
                    HttpResponseMessage result = Help.RunAsyncDELETEResponse(AuthHeader,BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs",
                           new StringContent("{\"" + tab_type + "\":[{\"tabId\":\"" + tab_id + "\"}]}", Encoding.UTF8, "application/json")
                        ).Result;
                   
                    if(result.IsSuccessStatusCode)
                    {
                        this.Status = result.ReasonPhrase;
                    }
                    else
                    {
                        Help.RaiseException(result);
                    }
                }
            }
            else
            {
                Help.RaiseException(recipient_tabs_response);
            }
              

              
             

             return this;
          }

          #endregion
    }//class
}//namespace
