﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("Account", "Account", "Account Service Object provide methods to work with DocuSign Account")]
    class Account
    {
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }

        private DataConnector _data_connector;

        #region Service Instance Fields
        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName; }
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }

        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }

        #endregion
        #region Class Level Fields

        private string envelopeID = "";
        private string envelope_status = "";
        private string name = "";
        private string _value = "";
        private string attached_file;
        private string recipient_id = "";
        private string file_name = "";

        private string tabID = "";
        private string tabLabel = "";
        private string jSONTabs = "";
        private string status = "";

        private string limit = "";
        private string remaining = "";
        private string reset_time = "";

        #endregion

        #region SMO properties

        [Attributes.Property("Limit", SoType.Number, "Limit", "API calls Limit")]
        public string Limit
        {
            get { return limit; }
            set { limit = value; }
        }

        [Attributes.Property("Remaining", SoType.Number, "Remaining", "Remaining API calls")]
        public string Remaining
        {
            get { return remaining; }
            set { remaining = value; }
        }

        [Attributes.Property("ResetTime", SoType.Text, "ResetTime", "Time left to reset API calls")]
        public string ResetTime
        {
            get { return reset_time; }
            set { reset_time = value; }
        }

        [Attributes.Property("Status", SoType.Text, "Status", "Status")]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        [Attributes.Property("JSONTabs", SoType.Text, "JSONTabs", "JSONTabs")]
        public string JSONTabs
        {
            get { return jSONTabs; }
            set { jSONTabs = value; }
        }

        [Attributes.Property("TabID", SoType.Text, "TabID", "TabID")]
        public string TabID
        {
            get { return tabID; }
            set { tabID = value; }
        }

        [Attributes.Property("TabLabel", SoType.Text, "TabLabel", "TabLabel")]
        public string TabLabel
        {
            get { return tabLabel; }
            set { tabLabel = value; }
        }

        [Attributes.Property("RecipientID", SoType.Text, "RecipientID", "RecipientID")]
        public string RecipientID
        {
            get { return recipient_id; }
            set { recipient_id = value; }

        }

        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }
        #endregion

        #region Get Remaining API Calls
        [Attributes.Method("GetApiCallsLimit", MethodType.Read, "Get Api Calls Limit", "Get Api Calls Limit",
            new string[] {  },
            new string[] {  },
            new string[] { "Limit", "Remaining", "ResetTime" })]
        public Account GetApiCallsLimit()
        {
           HttpResponseMessage folders = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/folders").Result;

            if(folders.StatusCode == HttpStatusCode.OK)
            {
                HttpHeaders headers = folders.Headers;
                IEnumerable<string> values;

                if(headers.TryGetValues("X-RateLimit-Limit", out values))
                {
                    this.Limit = values.First();
                }

                if (headers.TryGetValues("X-RateLimit-Remaining", out values))
                {
                    this.Remaining = values.First();
                }
                if (headers.TryGetValues("X-RateLimit-Reset", out values))
                {
                    TimeSpan t = TimeSpan.FromMilliseconds(Convert.ToDouble(values.First()));
                    this.ResetTime = string.Format("{0:D2}m:{1:D2}s",
                        t.Minutes,
                        t.Seconds
                        );
                }

            }
            else
            {
                Help.RaiseException(folders);
            }

            return this; 
        }
        #endregion

    }//class
}//namespace
