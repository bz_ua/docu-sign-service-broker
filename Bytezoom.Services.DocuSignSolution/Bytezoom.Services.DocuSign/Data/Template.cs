﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("Template", "Template", "Template Service Object provide methods to work with DocuSign Templates")]
    class Template
    {



        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }


        #region Class Level Fields

        #region Private Fields

        private string envelopeID = "";
        private string templateID = "";
        private string documentID = "";
        private string document = "";
        private string status = "";


        #endregion
        #endregion

        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName;}
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }
        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }

        [Attributes.Property("Document", SoType.File, "Document", "Document")]
        public string Document
        {
            get { return document; }
            set { document = value; }
        }

        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }
        [Attributes.Property("TemplateID", SoType.Text, "TemplateID", "TemplateID")]
        public string TemplateID
        {
            get { return templateID; }
            set { templateID = value; }
        }
        [Attributes.Property("DocumentID", SoType.Text, "DocumentID", "DocumentID")]
        public string DocumentID
        {
            get { return documentID; }
            set { documentID = value; }
        }
        [Attributes.Property("Status", SoType.Text, "Status", "Status")]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }


        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public Template()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region Apply To Document
        [Attributes.Method("ApplyToDocument", MethodType.Read, "Apply To Document", "Apply Templeta to Document",
            new string[] { "EnvelopeID", "TemplateID", "DocumentID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "EnvelopeID", "TemplateID", "DocumentID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "Status" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public Template ApplyToDocument()
        {

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/documents").Result;

            if(result.IsSuccessStatusCode)
            {
                JObject documents = JObject.Parse(Help.ReadContentAsString(result).Result);
                string start_page = "1";
                string end_page = "";
                foreach (dynamic subobj in documents["envelopeDocuments"])
                {
                    if (subobj["documentId"].ToString() == DocumentID)
                    {
                        end_page = subobj["pages"].ToString();
                        break;
                    }

                }
                HttpResponseMessage apply_to_document_response = Help.RunAsyncPostResponse( AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/documents/" + DocumentID + "/templates",
                    new StringContent(
                     "{ \"documentTemplates\": [ { \"documentId\": \"" + DocumentID + "\", \"templateId\": \"" + TemplateID + "\", \"documentStartPage\": \"" + start_page + "\", \"documentEndPage\": \"" + end_page + "\" } ] }", Encoding.UTF8, "application/json")
                    ).Result;

                if(apply_to_document_response.IsSuccessStatusCode)
                {
                    this.Status = apply_to_document_response.ReasonPhrase;
                }
                else
                {
                    Help.RaiseException(apply_to_document_response);
                }
            }
            {
                Help.RaiseException(result);
            }




            return this;
        }
        #endregion

        #region Delete Template
        [Attributes.Method("DeleteTemplate", MethodType.Execute, "Delete Template", "Delete Template",
            new string[] { "TemplateID" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "TemplateID" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "Status" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public Template DeleteTemplate()
        {

            //string recylebindID = "";

            //HttpResponseMessage Folders_response = Help.RunAsyncGetResponse(AuthHeader,BaseUrl + "/folders/").Result;
            //if(Folders_response.IsSuccessStatusCode)
            //{
            //    JObject Folders = JObject.Parse(Help.ReadContentAsString(Folders_response).Result);
            //    if (Folders["folders"] != null)
            //        foreach (var item in Folders["folders"])
            //        {
            //            if (item["type"].ToString() == "recyclebin")
            //            {
            //                recylebindID = item["folderId"].ToString();
            //                break;
            //            }
            //        }
            //}
            //else
            //{
            //    Help.RaiseException(Folders_response);
            //}


            HttpResponseMessage result = Help.RunAsyncPUTResponse(AuthHeader,BaseUrl + "/folders/" + ServiceConfiguration["ReycleBin Folder ID"].ToString(),
                  new StringContent("{\"envelopeIds\":[\""+TemplateID+"\"]}", Encoding.UTF8, "application/json")).Result;

            if (result.StatusCode == HttpStatusCode.OK)
            {
                this.Status = "OK";
            }
            else
            {
                Help.RaiseException(result);
                
            }


            return this;
        }
        #endregion


        #region CopyTemplateWithNewDocument
        [Attributes.Method("CopyTemplateWithNewDocument", MethodType.Read, "Copy Template With New Document", "CopyTemplateWithNewDocument",
            new string[] { "TemplateID", "Document" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "TemplateID", "Document" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "TemplateID" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public Template CopyTemplateWithNewDocument()
        {
            XElement attached_file;
            string base64;
            string file_name;
            JObject ExistingTemplate;

            try
            {
                attached_file = XElement.Parse(Document);
                base64 = attached_file.Element("content").Value;
                file_name = attached_file.Element("name").Value;
            }
            catch(Exception ex)
            {
                throw new Exception("Error parsing XML. "+ex.Message);
            }
          

            HttpResponseMessage existing_template_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/templates/" + TemplateID).Result;

            if (existing_template_response.IsSuccessStatusCode)
            {
                ExistingTemplate = JObject.Parse(Help.ReadContentAsString(existing_template_response).Result);
                JObject envelopeDefinition = (JObject)ExistingTemplate["envelopeTemplateDefinition"];
                envelopeDefinition.Property("templateId").Remove();

                string template_name = ExistingTemplate["envelopeTemplateDefinition"]["name"].ToString();
                ExistingTemplate["envelopeTemplateDefinition"]["name"] = template_name + "_" + file_name;
                ExistingTemplate["documents"][0]["name"] = file_name;
                ExistingTemplate["documents"][0]["documentBase64"] = base64;
                foreach (var item in ExistingTemplate["recipients"]["signers"][0]["tabs"]["radioGroupTabs"])
                {
                    foreach (var radio_button in item["radios"])
                    {
                        radio_button["anchorCaseSensitive"] = "false";
                    }
                }

                var Json = ExistingTemplate.ToString(Newtonsoft.Json.Formatting.None);

                HttpResponseMessage result = Help.RunAsyncPostResponse(AuthHeader, BaseUrl + "/templates", new StringContent(Json, Encoding.UTF8, "application/json")).Result;
                if(result.IsSuccessStatusCode)
                {
                    JObject created_template = JObject.Parse(Help.ReadContentAsString(result).Result);
                    this.TemplateID = created_template["templateId"].ToString();
                }
                else
                {
                    Help.RaiseException(result);
                }
            
            }
            else
            {
                Help.RaiseException(existing_template_response);
            }

            return this;
        }
        #endregion

        #endregion
    }
}