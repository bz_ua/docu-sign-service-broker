﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace Bytezoom.Services.DocuSign.Data
{

   class Help
    {

       
       public static string GetAuthHeader(string _UserName,string _Password,string _IntegratorKey)
       {
           return "{ \"Username\":\"" + _UserName + "\", \"Password\":\"" + _Password + "\", \"IntegratorKey\":\"" + _IntegratorKey + "\" }";
       }

 
       
        public static async Task<dynamic> RunAsyncPostResponse(string aut_header, string url, StringContent content)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.PostAsync(url, content);
            return response;
        }


        public static async Task<HttpResponseMessage> RunAsyncPUTResponse(string aut_header, string url, StringContent content)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.PutAsync(url, content);
            //string responseBody = await response.Content.ReadAsStringAsync();
            return response;
        }


        public static async Task<HttpResponseMessage> RunAsyncDELETEResponse(string aut_header, string url, StringContent content)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(url),
                Content = content
            };
           return await client.SendAsync(request);

        }


        public static async Task<HttpResponseMessage> RunAsyncGetResponse(string aut_header, string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(url);

            return response;
        }


        public static async Task<HttpResponseMessage> GetBaseUrlResponse(string aut_header, string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", aut_header);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return await client.GetAsync(url + "/restapi/v2/login_information");
        }

        public static async Task<string> ReadContentAsString(HttpResponseMessage response)
        {
            string result = await response.Content.ReadAsStringAsync();
            return result;
        }

        public static async Task<byte[]> ReadContentAsByteArray(HttpResponseMessage response)
        {
            return await response.Content.ReadAsByteArrayAsync();
        }


        public static Exception RaiseException(HttpResponseMessage response)
        {
            if (response.Content != null)
            {
                JObject content = JObject.Parse(Help.ReadContentAsString(response).Result);
                throw new Exception("Code:" + content["errorCode"].ToString() + ". Message: " + content["message"].ToString());
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }
        }


    }
}
