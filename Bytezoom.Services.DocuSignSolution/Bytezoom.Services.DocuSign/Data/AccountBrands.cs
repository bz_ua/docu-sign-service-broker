﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("AcountBands", "AcountBands", "AcountBands Service Object provide methods to work with DocuSign AcountBands")]
    class AcountBands
    {
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }

        private DataConnector _data_connector;

        #region Class Level Fields

        private string envelopeID = "";
        private string envelope_status = "";
        private string name = "";
        private string _value = "";
        private string attached_file;
        private string recipient_id = "";
        private string file_name = "";

        private string tabID = "";
        private string tabLabel = "";
        private string jSONTabs = "";
        private string status = "";

        #endregion

        #region SMO properties

        [Attributes.Property("Status", SoType.Text, "Status", "Status")]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        [Attributes.Property("JSONTabs", SoType.Text, "JSONTabs", "JSONTabs")]
        public string JSONTabs
        {
            get { return jSONTabs; }
            set { jSONTabs = value; }
        }

        [Attributes.Property("TabID", SoType.Text, "TabID", "TabID")]
        public string TabID
        {
            get { return tabID; }
            set { tabID = value; }
        }

        [Attributes.Property("TabLabel", SoType.Text, "TabLabel", "TabLabel")]
        public string TabLabel
        {
            get { return tabLabel; }
            set { tabLabel = value; }
        }

        [Attributes.Property("RecipientID", SoType.Text, "RecipientID", "RecipientID")]
        public string RecipientID
        {
            get { return recipient_id; }
            set { recipient_id = value; }

        }

        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }
        #endregion


        #region HelperMethods

        static async Task<dynamic> RunAsync(string user_name, string pass, string integrator_key, string docu_sign_url)
        {
            bool flag = false;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-DocuSign-Authentication", "{ \"Username\":\"" + user_name + "\", \"Password\":\"" + pass + "\", \"IntegratorKey\":\"" + integrator_key + "\" }");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(docu_sign_url + "/restapi/v2/login_information");
            string responseBody = await response.Content.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.OK)
                flag = true;
            else
            {
                flag = false;
                // dynamic data = JObject.Parse(responseBody);
                //AccountID = data["loginAccounts"][0]["accountId"].ToString();
            }
            return JObject.Parse(responseBody);
        }
        #endregion
    }//class
}//namespace
