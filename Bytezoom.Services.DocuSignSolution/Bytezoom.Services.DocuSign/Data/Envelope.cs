﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using System.Xml.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;


namespace Bytezoom.Services.DocuSign.Data
{
    [Attributes.ServiceObject("Envelope", "Envelope", "Envelope Service Object provide methods to work with DocuSign Envelope")]
    class Envelope
    {
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }

        #region Service Instance Fields
        public string UserName
        {
            get { return ServiceConfiguration.ServiceAuthentication.UserName; }
        }

        public string Password
        {
            get { return ServiceConfiguration.ServiceAuthentication.Password; }
        }

        public string IntegratorKey
        {
            get { return ServiceConfiguration["Integrator Key"].ToString(); }
        }

        public string BaseUrl
        {
            get { return ServiceConfiguration["Base Url"].ToString(); }
        }

        public string AuthHeader
        {
            get { return Help.GetAuthHeader(UserName, Password, IntegratorKey); }
        }

        #endregion

        #region Class Level Fields

        private string envelopeID = "";
        private string envelope_status = "";
        private string name = "";
        private string _value = "";
        private string attached_file;
        private string recipient_id = "";
        private string file_name = "";
        private string attachment_file_label = "";
        private string input_string = "";
        private string template_id = "";
        private string template_name = "";

        #endregion

        #region SMO properties

        [Attributes.Property("InputString", SoType.Text, "InputString", "InputString")]
        public string InputString
        {
            get { return input_string; }
            set { input_string = value; }
        }

        [Attributes.Property("AttachedFileLabel", SoType.Text, "AttachedFileLabel", "AttachedFileLabel")]
        public string AttachedFileLabel
        {
            get { return attachment_file_label; }
            set { attachment_file_label = value; }
        }


        [Attributes.Property("EnvelopeID", SoType.Text, "EnvelopeID", "EnvelopeID")]
        public string EnvelopeID
        {
            get { return envelopeID; }
            set { envelopeID = value; }
        }

        [Attributes.Property("Name", SoType.Text, "Name", "Name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Attributes.Property("AttachedFile", SoType.File, "AttachedFile", "AttachedFile")]
        public string AttachedFile
        {
            get { return attached_file; }
            set { attached_file = value; }
        }

        [Attributes.Property("Value", SoType.Text, "Value", "Value")]
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }


        [Attributes.Property("EnvelopeStatus", SoType.Text, "EnvelopeStatus", "EnvelopeStatus")]
        public string EnvelopeStatus
        {
            get { return envelope_status; }
            set { envelope_status = value; }

        }

        [Attributes.Property("RecipientID", SoType.Text, "RecipientID", "RecipientID")]
        public string RecipientID
        {
            get { return recipient_id; }
            set { recipient_id = value; }

        }
        [Attributes.Property("FileName", SoType.Text, "FileName", "FileName")]
        public string FileName
        {
            get { return file_name; }
            set { file_name = value; }

        }

        [Attributes.Property("TemplateID", SoType.Text, "TemplateID", "TemplateID")]
        public string TemplateID
        {
            get { return template_id; }
            set { template_id = value; }

        }

        [Attributes.Property("TemplateName", SoType.Text, "TemplateName", "TemplateName")]
        public string TemplateName
        {
            get { return template_name; }
            set { template_name = value; }

        }

        #endregion

        #region GetEnvelopeFormData method
        [Attributes.Method("GetEnvelopeFormData", MethodType.List, "Get Envelope Form Data", "Get Envelope Form Data",
            new string[] { "EnvelopeID"}, 
            new string[] { "EnvelopeID"},
            new string[] { "Name","Value" })]
        public List<Envelope> GetEnvelopeFormData()
        {
            List<Envelope> FormData = new List<Envelope>();

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/form_data").Result;
            
            if(result.StatusCode == HttpStatusCode.OK)
            {
                JObject form_data = JObject.Parse(Help.ReadContentAsString(result).Result);
                foreach (var subobj in form_data["formData"])
                {
                    Envelope env = new Envelope();
                    env.Name = subobj["name"].ToString();
                    env.Value = subobj["value"].ToString();
                    FormData.Add(env);
                    env = null;
                }
            }
            else
            {
                Help.RaiseException(result);
            }
            return FormData;
        }
        #endregion

        #region GetEnvelopeFormDataOneString
        [Attributes.Method("GetEnvelopeFormDataOneString", MethodType.List, "Get Envelope Form Data One String", "Get Envelope Form Data One String",
            new string[] { "EnvelopeID" },
            new string[] { "EnvelopeID" },
            new string[] { "Value" })]
        public Envelope GetEnvelopeFormDataOneString()
        {
            Dictionary<string, string> form_data_dict = new Dictionary<string, string>();

            HttpResponseMessage result = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/form_data").Result;

            if (result.IsSuccessStatusCode)
            {
                JObject form_data = JObject.Parse(Help.ReadContentAsString(result).Result);
                foreach (var subobj in form_data["formData"])
                {
                    form_data_dict.Add(subobj["name"].ToString(), subobj["value"].ToString());
                }
            }
            else
            {
                Help.RaiseException(result);
            }
            if(form_data_dict.Count>0)
            {
                this.Value = string.Join(";", form_data_dict.Select(x=> x.Key+":"+x.Value));
            }

            return this;
        }
        #endregion

        #region Extract Value From Input String
        [Attributes.Method("ExtractValueFromInputString", MethodType.Read, "Extract Value From Input String", "Extract Value From the string returned by GetEnvelopeFormDataOneString",
            new string[] { "InputString","Name" },
            new string[] { "InputString","Name" },
            new string[] { "Value" })]
        public Envelope ExtractValueFromInputString()
        {
            Dictionary<string, string> form_data_dict = InputString.Split(';').Select(s=> s.Split(':')).ToDictionary(p=>p[0].Trim(),p=>p[1].Trim());

            if (form_data_dict.ContainsKey(Name))
            {
                this.Value = form_data_dict[Name];
            }
            else
            {
                throw new Exception("The given field "+Name+" is not presented in the inpur string");
            }

            return this;
        }
        #endregion

        [Attributes.Method("GetAttachedFile", MethodType.Read, "Get Attached File", "Get Attached File",
            new string[] { "EnvelopeID", "RecipientID", "AttachedFileLabel" },
            new string[] { "EnvelopeID", "RecipientID", "AttachedFileLabel" },
            new string[] { "AttachedFile" })]
         public Envelope GetAttachedFile()
         {
             string attached_file_id="";
             string attached_file_name = "";
             string recipient_attachment_tab_id="";

             HttpResponseMessage recipient_tabs_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/recipients/" + RecipientID + "/tabs").Result;

            if (recipient_tabs_response.StatusCode == HttpStatusCode.OK)
            {
                JObject recipient_tabs = JObject.Parse(Help.ReadContentAsString(recipient_tabs_response).Result);

                foreach (var subobj in recipient_tabs["signerAttachmentTabs"])
                {
                    if (subobj["tabLabel"].ToString() == AttachedFileLabel)
                    {
                        recipient_attachment_tab_id = subobj["tabId"].ToString();
                        break;
                    }
                }
            }
            else
            {
                Help.RaiseException(recipient_tabs_response);
            }

            
            
            this.AttachedFile = null;

             if (!String.IsNullOrEmpty(recipient_attachment_tab_id))
             {
                HttpResponseMessage envelope_documents_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/documents/").Result;

                if (envelope_documents_response.StatusCode == HttpStatusCode.OK)
                {
                    JObject envelope_documents = JObject.Parse(Help.ReadContentAsString(envelope_documents_response).Result);
                    foreach (dynamic subobj in envelope_documents["envelopeDocuments"])
                    {
                        if (subobj["attachmentTabId"] != null)
                        {
                            if (subobj["attachmentTabId"].ToString() == recipient_attachment_tab_id)
                            {
                                attached_file_id = subobj["documentId"].ToString();
                                attached_file_name = subobj["name"].ToString();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Help.RaiseException(envelope_documents_response);
                }
                 

                 if(!String.IsNullOrEmpty(attached_file_id))
                 {
                    HttpResponseMessage attached_file_response = Help.RunAsyncGetResponse(AuthHeader, BaseUrl + "/envelopes/" + EnvelopeID + "/documents/" + attached_file_id).Result;
                    if(attached_file_response.StatusCode == HttpStatusCode.OK)
                    {
                        byte[] attached_file = Help.ReadContentAsByteArray(attached_file_response).Result;
                        string base64Code = Convert.ToBase64String(attached_file);
                        XElement exFile = new XElement("file",
                                new XElement("name", attached_file_name),
                                new XElement("content", base64Code)
                            );
                        this.AttachedFile = exFile.ToString();
                    }
                    else
                    {
                        Help.RaiseException(attached_file_response);
                    }
                     
                 }
             }
             return this;
         }

    }//class
}//namespace
